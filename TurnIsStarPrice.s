; NAME: TurnIsStarPrice
; GAMES: MP1_USA
; EXECUTION: Direct

ADDIU SP SP -36 ; Including 4 byte pad
SW RA 28(SP)
SW S6 24(SP) ;stars of player (address)
SW S5 20(SP) ;stars of player (value)
SW S4 16(SP) ;player has enough coins (1 = true)
SW S3 12(SP) ;player port
SW S2 8(SP) ;current turn
SW S1 4(SP) ;window handle
SW S0 0(SP) ;window handle

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;; load current turn ;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
LUI AT 0x800E     ;ram location of current turn part 1
ORI T2 AT 0xD5C9  ;ram location of current turn part 2
LBU S2 0(T2)      ;S2=current turn

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; load stars of player who initiated event ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
JAL GetCurrentPlayerIndex ;player1 = 0, ...
NOP

;player1
ADDIU T1 R0 0
BNE V0 T1 player2      ;V0 = result of GetCurrentPlayerIndex
NOP
LUI S6 hi(p1_stars)
ADDIU S6 S6 lo(p1_stars)
LBU S5 1(S6)
ADDIU S3 R0 0
J enoughcoinscheck
NOP

player2:
ADDIU T1 T1 1
BNE V0 T1 player3
NOP
LUI S6 hi(p2_stars)
ADDIU S6 S6 lo(p2_stars)
LBU S5 1(S6)
ADDIU S3 R0 1
J enoughcoinscheck
NOP

player3:
ADDIU T1 T1 1
BNE V0 T1 player4
NOP
LUI S6 hi(p3_stars)
ADDIU S6 S6 lo(p3_stars)
LBU S5 1(S6)
ADDIU S3 R0 2
J enoughcoinscheck
NOP

player4:
ADDIU T1 T1 1
LUI S6 hi(p4_stars)
ADDIU S6 S6 lo(p4_stars)
LBU S5 1(S6)
ADDIU S3 R0 3

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;; does the player have enough coins ;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
enoughcoinscheck:
ADDU A0 R0 S3
JAL PlayerHasCoins
ADDU A1 R0 S2

ADDU S4 R0 V0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;; display text message ;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ADDIU A0 R0 30 ; Upper left X coordinate
ADDIU A1 R0 42 ; Upper left Y coordinate
ADDIU A2 R0 14 ; Characters on X axis
JAL CreateTextWindow
ADDIU A3 R0 3 ; Characters on Y axis

ADDU S1 V0 R0 ; Save window handle (V0) into S0 and S1
SLL S0 S1 0x10
SRA S0 S0 0x10

ADDU A0 S0 R0 ; Window handle

ADDU T0 R0 R0
BEQ S4 T0 notenough
NOP
LUI A1 hi(msgenoughmoney)
ADDIU A1 A1 lo(msgenoughmoney)
J skipnotenough
NOP

notenough:
LUI A1 hi(msgnotenoughmoney)
ADDIU A1 A1 lo(msgnotenoughmoney)

skipnotenough:
ADDIU A2 R0 -1
JAL LoadStringIntoWindow
ADDIU A3 R0 -1

ADDU A0 S0 R0
JAL SetTextCharsPerFrame ; 0 frames between characters
ADDU A1 R0 R0

JAL ShowTextWindow
ADDU A0 S1 R0

;;; is player a cpu -> auto cancel text
JAL GetCurrentPlayerIndex ;player1 = 0, ...
NOP
JAL PlayerIsCPU ;return value 1 means true
ADDU A0 V0 R0

ADDIU T6 R0 1
BEQ V0 T6 acpu
NOP
JAL WaitForTextConfirmation
ADDU A0 S1 R0
J hide
NOP

acpu:
JAL SleepProcess
ADDIU A0 R0 30
;;; is player a cpu -> auto cancel text

hide:
JAL HideTextWindow
ADDU A0 S1 R0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;; Subtract coins ;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ADDU T0 R0 R0
BEQ S4 T0 notenoughmoney
NOP

SUBU T0 R0 S2                ;negate turn number
ADDU A0 R0 S3
JAL AdjustPlayerCoinsGradual
ADDU A1 R0 T0                ;so it will be subtracted

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;; Give star ;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ADDIU S5 S5 1 ;star count + 1 new star
SB S5 1(S6)  ;store in star count address

JAL SleepProcess
ADDIU A0 R0 30

notenoughmoney:
;NOP

LW RA 28(SP)
LW S6 24(SP)
LW S5 20(SP)
LW S4 16(SP)
LW S3 12(SP)
LW S2 8(SP)
LW S1 4(SP)
LW S0 0(SP)
JR RA
ADDIU SP SP 36

.align 16
msgenoughmoney:
.byte 0x08 ; white
.ascii "The turn is the price"
.byte 0xC2 ; !
.byte 0x0A ; \n
.ascii "Take this star"
.byte 0xC2 ; !
.byte 0xFF,0 ; FF=Pause

.align 16
msgnotenoughmoney:
.byte 0x08 ; white
.ascii "The turn is the price"
.byte 0xC2 ; !
.byte 0x0A ; \n
.ascii "Sadly"
.byte 0x82 ; comma
.ascii " you don"
.byte 0x5C ; inverted comma
.ascii "t"
.byte 0x0A ; \n
.ascii "have enough coins"
.byte 0x85 ; period
.byte 0xFF,0 ; FF=Pause































