; NAME: LoseACoin
; GAMES: MP1_USA
; EXECUTION: Direct

ADDIU SP SP -4
SW RA 0(SP)

; which player initiated the event
JAL GetCurrentPlayerIndex ;player1 = 0, ...
NOP

finally:
ADDU A0 V0 R0
JAL AdjustPlayerCoinsGradual ;A0 = player
ADDIU A1 R0 -1               ;A1 = coins gained

JAL SleepProcess
ADDIU A0 R0 4

LW RA 0(SP)
JR RA
ADDIU SP SP 4































