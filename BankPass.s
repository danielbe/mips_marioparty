; NAME: BankPass
; GAMES: MP1_USA
; EXECUTION: Direct

ADDIU SP SP -24 ; Including 4 byte pad
SW RA 20(SP)
SW S4 16(SP) ;coins of initiater
SW S3 12(SP) ;player port
SW S2 8(SP)  ;coins to deposit
SW S1 4(SP)  ;window handle
SW S0 0(SP)  ;window handle

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; load coins of player who initiated event ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;coin count is necessary to check how many coins
;the bank will get
JAL GetCurrentPlayerIndex ;player1 = 0, ...
NOP

;player1
ADDU T1, R0, R0
BNE V0, T1, player2      ;V0 = result of GetCurrentPlayerIndex
NOP
LUI T5 hi(p1_coins)
ADDIU T5 T5 lo(p1_coins)
LBU S4, 1(T5)
ADDIU S3 R0 0
J coincount
NOP

player2:
ADDIU T1, T1, 1
BNE V0, T1, player3
NOP
LUI T5 hi(p2_coins)
ADDIU T5 T5 lo(p2_coins)
LBU S4, 1(T5)
ADDIU S3 R0 1
J coincount
NOP

player3:
ADDIU T1, T1, 1
BNE V0, T1, player4
NOP
LUI T5 hi(p3_coins)
ADDIU T5 T5 lo(p3_coins)
LBU S4, 1(T5)
ADDIU S3 R0 2
J coincount
NOP

player4:
ADDIU T1, T1, 1
LUI T5 hi(p4_coins)
ADDIU T5 T5 lo(p4_coins)
LBU S4, 1(T5)
ADDIU S3 R0 3

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;; how many coins to give bank ;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
coincount:
;fourcoins
ADDIU S2, R0, 4
BNE S4, S2, threecoins
NOP
J display
NOP

threecoins:
ADDIU S2, R0, 3
BNE S4, S2, twocoins
NOP
J display
NOP

twocoins:
ADDIU S2, R0, 2
BNE S4, S2, onecoin
NOP
J display
NOP

onecoin:
ADDIU S2, R0, 1
BNE S4, S2, zerocoins
NOP
J display
NOP

zerocoins:
ADDIU S2, R0, 0
BNE S4, S2, fivecoins
NOP
J display
NOP

fivecoins:
ADDIU S2, R0, 5

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;; display text message ;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
display:
ADDIU A0 R0 30 ; Upper left X coordinate
ADDIU A1 R0 42 ; Upper left Y coordinate
ADDIU A2 R0 17 ; Characters on X axis
JAL CreateTextWindow
ADDIU A3 R0 3 ; Characters on Y axis

ADDU S1 V0 R0 ; Save window handle (V0) into S0 and S1
SLL S0 S1 0x10
SRA S0 S0 0x10

ADDU A0 S0 R0 ; Window handle

ADDIU T4, R0, 0
;money:
BEQ S2, T4, nomoney
LUI A1 hi(coinsYes)
ADDIU A1 A1 lo(coinsYes)
J window
NOP
nomoney:
LUI A1 hi(coinsNo)
ADDIU A1 A1 lo(coinsNo)

window:
ADDIU A2 R0 -1
JAL LoadStringIntoWindow
ADDIU A3 R0 -1

ADDU A0 S0 R0
JAL SetTextCharsPerFrame ; 0 frames between characters
ADDU A1 R0 R0

JAL ShowTextWindow
ADDU A0 S1 R0

;;; is player a cpu -> auto cancel text
JAL GetCurrentPlayerIndex ;player1 = 0, ...
NOP
JAL PlayerIsCPU ;return value 1 means true
ADDU A0 V0 R0

ADDIU T6 R0 1
BEQ V0 T6 acpu
NOP
;notacpu:
JAL WaitForTextConfirmation
ADDU A0 S1 R0
J hide
NOP

acpu:
JAL SleepProcess
ADDIU A0 R0 30
;;; is player a cpu -> auto cancel text

hide:
JAL HideTextWindow
ADDU A0 S1 R0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;; save coin amount in ram ;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
finally:
;address used below is random
;no errors found in testing
LUI AT, 0x800F
ORI T2, AT, 0x0156
LBU T3, 1(T2)
ADDU T3, T3, S2
SB T3, 1(T2)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;; player loses the coins ;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ADDU A0 R0 S3
JAL AdjustPlayerCoinsGradual
;always subtracting 5 (coin player count cannot be negative)
ADDIU A1 R0 -5

JAL SleepProcess
ADDIU A0 R0 20

LW RA 20(SP)
LW S4 16(SP)
LW S3 12(SP)
LW S2 8(SP)
LW S1 4(SP)
LW S0 0(SP)
JR RA
ADDIU SP SP 24

.align 16
coinsYes:
.byte 0x08 ; white
.ascii "This is Koopa Bank"
.byte 0x85 ; period
.byte 0x0A ; \n
.ascii "Please deposit "
.byte 0x03 ; red
.ascii "5 Coins"
.byte 0x0A ; \n
.byte 0x08 ; white
.ascii "into our savings account"
.byte 0x85 ; period
.byte 0xFF,0 ; FF=Pause

.align 16
coinsNo:
.byte 0x08 ; white
.ascii "This is Koopa Bank"
.byte 0x85 ; period
.byte 0x0A ; \n
.ascii "I" 
.byte 0x5C ; inverted comma
.ascii "d ask you to make a deposit"
.byte 0x82 ; comma
.byte 0x0A ; \n
.ascii "but you have no Coins to deposit"
.byte 0xC2 ; !
.byte 0xFF,0 ; FF=Pause































