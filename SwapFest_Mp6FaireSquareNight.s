; NAME: SwapFestMp6Night
; GAMES: MP1_USA
; EXECUTION: Direct

ADDIU SP SP -24 ; Including 4 byte pad
SW RA 16(SP)
SW S6 20(SP) ;star jackpot
SW S5 12(SP) ;UNUSED
SW S4 12(SP) ;UNUSED
SW S3 12(SP) ;winner character
SW S2 8(SP) ;winner port
SW S1 4(SP) ;window handle
SW S0 0(SP) ;window handle

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;; collect stars for jackpot ;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ADDU S6 R0 R0
ADDU T1 R0 R0

LUI T2 hi(p1_stars)
ADDIU T2 T2 lo(p1_stars) ;address of stars
ADDIU T4 T2 0xC0         ;offset times 4

player:
LBU T3 1(T2)             ;stars amount

BEQ T3 T1 nostars
NOP
ADDIU S6 S6 1            ;stars in jackpot
ADDIU T3 T3 -1           ;remove a star, if the player has at least one
SB T3 1(T2)
nostars:
ADDIU T2 T2 0x30         ;address of stars of next player
BNE T2 T4 player         ;exit when reached player 4
NOP

;no one has a star -> skip everything
BEQ S6 R0 message2start
NOP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;; display text message ;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
messagestart:
ADDIU A0 R0 30 ; Upper left X coordinate
ADDIU A1 R0 42 ; Upper left Y coordinate
ADDIU A2 R0 18 ; Characters on X axis
JAL CreateTextWindow
ADDIU A3 R0 2 ; Characters on Y axis

ADDU S1 V0 R0 ; Save window handle (V0) into S0 and S1
SLL S0 S1 0x10
SRA S0 S0 0x10

ADDU A0 S0 R0 ; Window handle

LUI A1 hi(msginitial)
ADDIU A1 A1 lo(msginitial)

ADDIU A2 R0 -1
JAL LoadStringIntoWindow
ADDIU A3 R0 -1

ADDU A0 S0 R0
JAL SetTextCharsPerFrame ; 0 frames between characters
ADDU A1 R0 R0

JAL ShowTextWindow
ADDU A0 S1 R0

JAL WaitForTextConfirmation
ADDU A0 S1 R0

JAL HideTextWindow
ADDU A0 S1 R0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;; Who wins the star ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
JAL SleepProcess ;to make it more exciting
ADDIU A0 R0 60

JAL guRandom ;generates random number in V0
NOP          ; probably between 0 and FFFFFFFF

SRL T0 V0 28      ;T0 now is between 0 and 3
ADDIU T2 R0 1     ;compared against

ADDIU S2 R0 0  ;player port (will be the winner)

ADDU T3 R0 R0
LUI T3 hi(p1_char)
ADDIU T3 T3 lo(p1_char)

wincheck:
SLT T1 T0 T2
BNE T1 R0 givestarstart
NOP
ADDIU S2 S2 1
ADDIU T2 T2 1
ADDIU T3 T3 0x30
J wincheck
NOP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;; Character of winner ;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
LBU T4 0(T3)

;to test what number we get from px_char
ADDIU A0 R0 0
JAL AdjustPlayerCoinsGradual
ADDU A1 R0 T4

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;; Give star ;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
givestarstart:
ADDU T0 R0 R0
ADDU T2 R0 R0
LUI T1 hi(p1_stars)
ADDIU T1 T1 lo(p1_stars) ;address of stars

setupstaraddress:
BEQ S2 T0 givestar
NOP
ADDIU T0 T0 1
ADDIU T1 T1 0x30
J setupstaraddress
NOP

givestar:
LBU T2 1(T1)
ADDU T2 T2 S6 ;star count + star jackpot

;overflow check
SRL T3 T2 8
BEQ T3 R0 checkfinished
NOP
ADDIU T2 R0 255

checkfinished:
SB T2 1(T1)  ;store in star count address

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; display text message pt2 ;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
message2start:
ADDIU A0 R0 30 ; Upper left X coordinate
ADDIU A1 R0 42 ; Upper left Y coordinate
ADDIU A2 R0 18 ; Characters on X axis
JAL CreateTextWindow
ADDIU A3 R0 2 ; Characters on Y axis

ADDU S1 V0 R0 ; Save window handle (V0) into S0 and S1
SLL S0 S1 0x10
SRA S0 S0 0x10

ADDU A0 S0 R0 ; Window handle

BNE S6 R0 atleastonestar
NOP
nostar:
LUI A1 hi(msgabort)
ADDIU A1 A1 lo(msgabort)
J afterstarcheck
NOP

atleastonestar:
LUI A1 hi(msgresult)
ADDIU A1 A1 lo(msgresult)

afterstarcheck:
ADDIU A2 R0 -1
JAL LoadStringIntoWindow
ADDIU A3 R0 -1

ADDU A0 S0 R0
JAL SetTextCharsPerFrame ; 0 frames between characters
ADDU A1 R0 R0

JAL ShowTextWindow
ADDU A0 S1 R0

JAL WaitForTextConfirmation
ADDU A0 S1 R0

JAL HideTextWindow
ADDU A0 S1 R0

LW RA 16(SP)
LW S3 12(SP)
LW S2 8(SP)
LW S1 4(SP)
LW S0 0(SP)
JR RA
ADDIU SP SP 24

.align 16
msginitial:
.byte 0x08 ; white
.ascii "Welcome to Swap Fest"
.byte 0x0A ; \n
.ascii "Everyone"
.byte 0x82 ; comma
.ascii " give me a star"
.byte 0x85 ; period
.byte 0xFF,0 ; FF=Pause

.align 16
msgresult:
.ascii "And the winner is"
.byte 0xFF,0 ; FF=Pause

.align 16
msgabort:
.ascii "I"
.byte 0x5C ; inverted comma
.ascii "d welcome you to Swap Fest"
.byte 0x82 ; comma
.byte 0x0A ; \n
.ascii "but noone has a single star"
.byte 0xC2 ; !
.byte 0xFF,0 ; FF=Pause

;http://davidlovesprogramming.blogspot.com/2013/05/string-manipulation-in-mips-part-3.html

.align 16
mario:
; color red
.ascii "Mario"
.byte 0x08 ; white
.byte 0xC2 ; !
.byte 0xFF,0 ; FF=Pause

.align 16
luigi:
; color green
.ascii "Luigi"
.byte 0x08 ; white
.byte 0xC2 ; !
.byte 0xFF,0 ; FF=Pause

.align 16
peach:
; color pink
.ascii "Peach"
.byte 0x08 ; white
.byte 0xC2 ; !
.byte 0xFF,0 ; FF=Pause

.align 16
yoshi:
; color light green
.ascii "Yoshi"
.byte 0x08 ; white
.byte 0xC2 ; !
.byte 0xFF,0 ; FF=Pause

.align 16
wario:
; color yellow
.ascii "Wario"
.byte 0x08 ; white
.byte 0xC2 ; !
.byte 0xFF,0 ; FF=Pause

.align 16
dk:
; color brown
.ascii "DK"
.byte 0x08 ; white
.byte 0xC2 ; !
.byte 0xFF,0 ; FF=Pause
























