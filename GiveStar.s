; NAME: GiveStar
; GAMES: MP1_USA
; EXECUTION: Direct
; PARAM: Number|stars

ADDIU SP SP -16 ; Including 4 byte pad
SW RA 8(SP)
SW S1 4(SP) ;window handle
SW S0 0(SP) ;window handle

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;; display text message ;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ADDIU A0 R0 30 ; Upper left X coordinate
ADDIU A1 R0 42 ; Upper left Y coordinate
ADDIU A2 R0 10 ; Characters on X axis
JAL CreateTextWindow
ADDIU A3 R0 2 ; Characters on Y axis

ADDU S1 V0 R0 ; Save window handle (V0) into S0 and S1
SLL S0 S1 0x10
SRA S0 S0 0x10

ADDU A0 S0 R0 ; Window handle

LUI A1 hi(initial)
ADDIU A1 A1 lo(initial)

ADDIU A2 R0 -1
JAL LoadStringIntoWindow
ADDIU A3 R0 -1

ADDU A0 S0 R0
JAL SetTextCharsPerFrame ; 0 frames between characters
ADDU A1 R0 R0

JAL ShowTextWindow
ADDU A0 S1 R0

;;; is player a cpu -> auto cancel text
JAL GetCurrentPlayerIndex ;player1 = 0, ...
NOP
JAL PlayerIsCPU ;return value 1 means true
ADDU A0 V0 R0

ADDIU T6 R0 1
BEQ V0 T6 acpu
NOP
;notacpu:
JAL WaitForTextConfirmation
ADDU A0 S1 R0
J hide
NOP

acpu:
JAL SleepProcess
ADDIU A0 R0 30
;;; is player a cpu -> auto cancel text

hide:
JAL HideTextWindow
ADDU A0 S1 R0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; load stars of player who initiated event ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
JAL GetCurrentPlayerIndex ;player1 = 0, ...
NOP

ADDU T1 R0 R0
LUI T0 hi(p1_stars)
ADDIU T0 T0 lo(p1_stars)

searchaddress:
BEQ T1 V0 flowcheck
NOP
ADDIU T1 T1 1
ADDIU T0 T0 0x30 ;address of stars of next player
J searchaddress
NOP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;; overflow / underflow check ;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
flowcheck:
LBU T2 1(T0)
ADDIU T1 R0 stars
ADDU T2 T2 T1

;stars < 0 could be an underflow
SLT T3 T1 R0
BNE T3 R0 underflowcheck
NOP

;overflowcheck:
;right shifted by 8 and still > 0
;means bigger than 255
;so that would be an overflow
SRL T5 T2 8
BEQ T5 R0 checkfinished
NOP
ADDIU T2 R0 255
J checkfinished
NOP

underflowcheck:
;right shifted by 31 and still > 0
;means less than 0
;so that would be an underflow
SRL T5 T2 31
BEQ T5 R0 checkfinished
NOP
ADDIU T2 R0 0

checkfinished:
SB T2 1(T0)

LW RA 8(SP)
LW S1 4(SP)
LW S0 0(SP)
JR RA
ADDIU SP SP 16

.align 16
initial:
.byte 0x08 ; white
.ascii "Here"
.byte 0x82 ; comma
.byte 0x0A ; \n
.ascii "take these stars"
.byte 0x85 ; period
.byte 0xFF,0 ; FF=Pause































