; NAME: BankLandOn
; GAMES: MP1_USA
; EXECUTION: Direct

ADDIU SP SP -20 ; Including 4 byte pad
SW RA 12(SP)
SW S2 8(SP) ;coins in bank
SW S1 4(SP) ;window handle
SW S0 0(SP) ;window handle

; which player initiated the event
JAL GetCurrentPlayerIndex ;player1 = 0, ...
NOP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;; get bank coin amount ;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
LUI AT, 0x800F     ;ram location of coins in bank part1
ORI T2, AT, 0x0156 ;ram location of coins in bank part2
LBU S2, 1(T2)
SB R0, 1(T2)       ;set coins in bank to zero

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;; display text message ;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ADDIU A0 R0 30 ; Upper left X coordinate
ADDIU A1 R0 42 ; Upper left Y coordinate
ADDIU A2 R0 22 ; Characters on X axis
JAL CreateTextWindow
ADDIU A3 R0 3 ; Characters on Y axis

ADDU S1 V0 R0 ; Save window handle (V0) into S0 and S1
SLL S0 S1 0x10
SRA S0 S0 0x10

ADDU A0 S0 R0 ; Window handle

;different text depending on coins
;money:
ADDIU T3 R0 0
BEQ S2, T3, nomoney
LUI A1 hi(moneyYes)
ADDIU A1 A1 lo(moneyYes)
J window
NOP
nomoney:
LUI A1 hi(moneyNo)
ADDIU A1 A1 lo(moneyNo)

window:
ADDIU A2 R0 -1
JAL LoadStringIntoWindow
ADDIU A3 R0 -1

ADDU A0 S0 R0
JAL SetTextCharsPerFrame ; 0 frames between characters
ADDU A1 R0 R0

JAL ShowTextWindow
ADDU A0 S1 R0

;;; is player a cpu -> auto cancel text
JAL GetCurrentPlayerIndex ;player1 = 0, ...
NOP
JAL PlayerIsCPU ;return value 1 means true
ADDU A0 V0 R0

ADDIU T6 R0 1
BEQ V0 T6 acpu
NOP
JAL WaitForTextConfirmation
ADDU A0 S1 R0
J hide
NOP

acpu:
JAL SleepProcess
ADDIU A0 R0 30
;;; is player a cpu -> auto cancel text

hide:
JAL HideTextWindow
ADDU A0 S1 R0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;; give coins to player ;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ADDU A1 R0 S2
JAL AdjustPlayerCoinsGradual
NOP

JAL SleepProcess
ADDIU A0 R0 30

LW RA 12(SP)
LW S2 8(SP)
LW S1 4(SP)
LW S0 0(SP)
JR RA
ADDIU SP SP 20

.align 16
moneyNo:
.byte 0x08 ; white
.ascii "As a "
.byte 0x06 ; cyan
.ascii "Special Bonus"
.byte 0x08 ; white
.byte 0x82 ; comma
.ascii " you get all the Coins"
.byte 0x0A ; \n
.ascii "currently in our reserves"
.byte 0xC2 ; !
.byte 0x0A ; \n
.byte 0x85 ; period
.byte 0x85 ; period
.byte 0x85 ; period
.ascii "Unfortunately"
.byte 0x82 ; comma
.ascii " we don"
.byte 0x5C ; inverted comma
.ascii "t have a single Coin"
.byte 0xC2 ; !
.byte 0xFF,0 ; FF=Pause

.align 16
moneyYes:
.byte 0x08 ; white
.ascii "As a "
.byte 0x06 ; cyan
.ascii "Special Bonus"
.byte 0x08 ; white
.ascii " for shopping at the bank"
.byte 0x82 ; comma
.byte 0x0A ; \n
.ascii "you get all the Coins in our reserves"
.byte 0xC2 ; !
.byte 0x0A ; \n
.ascii "Congratulations"
.byte 0xC2 ; !
.byte 0xFF,0 ; FF=Pause































